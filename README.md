# UNCAP Paperboy

The UNCAP Platform aggregator, as a NodeRED workflow.  
It is supposed to run in Raptorbox, a proprietary platform for the management of IoT devices. 

In addition to it, there are some simulators that can generate data as they were produced by:
- a Glucometer
- a Heart Rate tester
- an Oxygen Saturation tester

**This code will not run as it is since it is missing the needed credentials.**